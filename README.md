# Software Studio 2020 Spring Midterm Project


## Topic
* Project Name : chatroom
* Key functions (add/delete)

    1. 分為Public chatroom和Private Chatroom兩種，Private Chatroom有animal、forest、sky三種，分別要輸入相應的密碼才能進入。
    2. 有Signin、Signout和Google login。
    3. Chatroom裡面可以讀取訊息跟傳送訊息，在say something將訊息打入，並且按Send傳送。
    4. 可以讀到以前的訊息並且正確顯示。
    5. 傳html code可以正確顯示，不會影響到對話。
    6. 聊天室大小可以伸縮。

* Other functions (add/delete)
    1. 可以上傳圖片，點選選擇檔案並按Upload上傳。
    2. 可以用貼圖，按Sticker。
    3. 訊息有紀錄上傳時間。
    4. 如果有打開聊天室但沒有在當前頁面，會跳notification。
    5. 可以改username。
    6. 有Profile。
    7. 可以用臉書登入 (利用fb developer)。
    8. 有動畫。(按鈕以及Profile的loading和背景)。

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://chatroom-bebc8.web.app/

# Components Description : 
1. Membership Mechanism	 : 
在index.html頁面，有Sign in 和 Sign up功能。用New account創建新帳號，創建完後再用Sign in 登入。
2. Firebase Page :
用firebase deploy來建立在他人電腦也可使用的網頁。 
    
3. Database : 
可以在Profile更改displayName。displayName如果用臉書或Google登入的話會有自己的值，如果是自己創建的帳戶的話，diaplayName的default值我設成email中@前的字串。
在Chatroom時，可以讀取之前有誰在什麼時候發過訊息，並且發送我當前時間的訊息至聊天室。

4. RWD :
可以自由伸縮頁面，此功能我在不同的html上是用不同方式實作，flex box跟Bootstrap都有用到。
5. Topic Key Function：
可以在animal、forest、sky等三個private聊天室中和他人聊天，包括上傳訊息以及讀取所有歷史訊息。每則訊息的資訊包含：username、data(text or picture)、time。

6. Third-Party Sign In：
可以在index.html的地方用Google、FB帳號登入。Google的部分不用設定太多東西，FB的部分要從FB developer那邊取得應用程式ID和應用程式密鑰。

7. Chrome Notification：
在跳離頁面卻有人傳訊息的時候，會有Chrome Notification。Notification的格式為：username say : [something]。

8. Use CSS Animation：
Public Chatroom的按鈕按下去會有彩虹的效果，但是要長按比較明顯。其餘Public Chatroom會飄著泡泡。另外Profile頁面的loading條也有動畫，然後背景會隨時間變換。

# Other Functions Description : 
1. Profile :
具有Profile頁面，可以在裡面改username，然後裡面有一則不好笑的笑話。username更改後，Profile上的 "Hi, I am [username]"的username會跟著更改，chooseroom.html的頁面中左上方的username顯示也會跟著更改。 
原本想要做更改頭貼的功能，但Profile被我排外觀排太久(結果就沒時間)，所以他現在除了動畫多了點之外就只有改username的功能。

2. Sticker :
Chatroom裡面可以點Sticker，然後發出訊息會是一張有很多隻手的貼圖。原本想做可以在Chooseroom更換貼圖的功能但我來不及，所以它現在就只能一種貼圖。

3. Upload picture :
可以上傳圖片到聊天室，方法為選擇完圖片後按Upload。上傳的圖片大小我有做限制，所以不會讓圖片在聊天室中顯得過於大張，但須等待Storage拿到圖片所以需要一點上傳時間。
較好的做法應該是再做一條上傳進度條來確認上傳進度，但一樣，我這次來不及做QQ。
## Security Report (Optional)
1. Deal with messages when sending html code : 
可以在聊天室中傳送html code，不會影響到聊天室本身。